package org.silk.mastermind;

import java.util.Scanner;

public class ConsoleIOHandler implements IOHandler {
    private Scanner in;

    public ConsoleIOHandler() {
        in = new Scanner(System.in);
    }

    @Override
    public void write(String message, Object... arguments) {
        System.out.println(String.format(message, arguments));
    }

    @Override
    public String read() {
        return in.next();
    }
}
