package org.silk.mastermind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Game implements Runnable {
    private Solution solution;
    private List<Turn> turns;
    private int size;
    private int tries;
    private IOHandler ioHandler;
    private Pattern pattern;

    public Game(IOHandler ioHandler) {
        this.ioHandler = ioHandler;
        this.size = 4;
        this.tries = 10;
        Color[] values = Color.values();
        solution = new Solution(
                new Random().ints(size, 0, values.length)
                        .mapToObj(i -> values[i])
                        .collect(Collectors.toList())
        );
        pattern = Pattern.compile(String.format("^[%s]{%d}$", new Solution(Arrays.asList(values)), size));
        turns = new ArrayList<>(tries);
    }

    @Override
    public void run() {
        boolean found;
        do {
            ioHandler.write(toString());
            String input = null;
            boolean correct = false;
            while(!correct) {
                ioHandler.write("Vous >");
                input = ioHandler.read(pattern);
                correct = pattern.matcher(input).matches();
                if (!correct) {
                    ioHandler.write("Saisie fausse, respecter le pattern \"%s\"", pattern.pattern());
                }
            }
            found = submit(input);
        } while (!found && turns.size() < tries);
        if (found) {
            ioHandler.write("Bravo ! Vous avez gagné en %d tours", turns.size());
        } else {
            ioHandler.write("Dommage ! Vous avez perdu, la solution était %s", solution);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("|-------------------|\n");
        for (Turn turn : turns) {
            sb.append(turn).append("\n");
        }
        if (turns.size() < tries) {
            sb.append("|....| . | . | ").append(turns.size() + 1).append("/").append(tries).append(" |\n");
        }
        sb.append("|-------------------|");
        return sb.toString();
    }

    boolean submit(String s) {
        Turn turn = new Turn(s, turns.size() + 1, tries);
        turns.add(turn);
        return turn.compareTo(solution);
    }
}
