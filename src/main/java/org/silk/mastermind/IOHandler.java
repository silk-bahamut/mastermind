package org.silk.mastermind;

public interface IOHandler {
    void write(String message, Object... arguments);

    String read();
}
