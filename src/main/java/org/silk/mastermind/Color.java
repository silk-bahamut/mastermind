package org.silk.mastermind;

public enum Color {
    ROUGE('R'),
    JAUNE('J'),
    BLEU('B'),
    ORANGE('O'),
    VERT('V'),
    NOIR('N');

    private char display;

    Color(char display) {
        this.display = display;
    }

    public char getDisplay() {
        return display;
    }

    public static Color fromDisplay(char c) {
        Color out = null;
        for (Color color : Color.values()) {
            if (color.display == c) {
                out = color;
                break;
            }
        }
        if (out == null) {
            throw new IllegalArgumentException("Caractère non supporté : " + c);
        }
        return out;
    }
}
