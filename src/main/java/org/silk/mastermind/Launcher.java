package org.silk.mastermind;

public class Launcher {
    public static void main(String[] args) {
        new Game(new ConsoleIOHandler()).run();
    }
}
