package org.silk.mastermind;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {
    private List<Color> elements;

    public Solution(String s) {
        this(s.chars()
                .mapToObj(i -> Color.fromDisplay((char) i))
                .collect(Collectors.toList()));
    }

    public Solution(List<Color> elements) {
        this.elements = Collections.unmodifiableList(elements);
    }

    @Override
    public String toString() {
        return getElements()
                .stream()
                .map(c -> "" + c.getDisplay())
                .collect(Collectors.joining(""));
    }

    public List<Color> getElements() {
        return elements;
    }

    /*
    Count exact match (color and position) and remove them form the input lists
     */
    int countExact(List<Color> solutionDuplicate, List<Color> guessDuplicate) {
        int exact = 0;
        Iterator<Color> itSol = solutionDuplicate.iterator();
        Iterator<Color> itGuess = guessDuplicate.iterator();
        while (itGuess.hasNext()) {
            Color guess = itGuess.next();
            Color s = itSol.next();
            if (guess == s) {
                exact++;
                itGuess.remove();
                itSol.remove();
            }
        }
        return exact;
    }

    int countSameColor(List<Color> solutionDuplicate, List<Color> guessDuplicate) {
        int sameColor = 0;
        for (Color guess : guessDuplicate) {
            if (solutionDuplicate.contains(guess)) {
                sameColor++;
                solutionDuplicate.remove(guess);
            }
        }
        return sameColor;
    }
}
