package org.silk.mastermind;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class Turn extends Solution {
    private int exact;
    private int sameColor;
    private int turnNumber;
    private int maxTurn;

    public Turn(String s, int turnNumber, int maxTurn) {
        super(s);
        this.turnNumber = turnNumber;
        this.maxTurn = maxTurn;
    }

    public boolean compareTo(Solution solution) {
        if (solution.getElements().size() != getElements().size()) {
            throw new RuntimeException("Non comparable");
        }
        List<Color> solutionDuplicate = new ArrayList<>(solution.getElements());
        List<Color> guessDuplicate = new ArrayList<>(getElements());
        exact = countExact(solutionDuplicate, guessDuplicate);
        boolean finished = solutionDuplicate.isEmpty();
        sameColor = countSameColor(solutionDuplicate, guessDuplicate);
        return finished;
    }

    @Override
    public String toString() {
        return String.format("|%s| %d | %d | %d/%d |",
                super.toString(), exact, sameColor, turnNumber, maxTurn);
    }
}
