package org.silk.mastermind;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class SolutionTest {

    @Test
    public void test_convert_ok() {
        Solution s = new Solution("RJOBVN");
        Assertions.assertThat(s.getElements())
                .containsExactly(
                        Color.ROUGE,
                        Color.JAUNE,
                        Color.ORANGE,
                        Color.BLEU,
                        Color.VERT,
                        Color.NOIR
                );
    }

    @Test
    public void test_convert_ko() {
        Assertions.assertThatThrownBy(() -> new Solution("RJoBVN"))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Caractère non supporté : o");
    }

    @Test
    public void test_all_exact() {
        Solution s = new Solution("RJOBVN");
        Turn turn = new Turn("RJOBVN", 0, 0);
        boolean r = turn.compareTo(s);
        Assertions.assertThat(r).isTrue();
        Assertions.assertThat(turn.toString()).isEqualTo("|RJOBVN| 6 | 0 | 0/0 |");
    }

    @Test
    public void test_all_same_color() {
        Solution s = new Solution("RJOBVN");
        Turn turn = new Turn("NRJOBV", 0, 0);
        boolean r = turn.compareTo(s);
        Assertions.assertThat(r).isFalse();
        Assertions.assertThat(turn.toString()).isEqualTo("|NRJOBV| 0 | 6 | 0/0 |");
    }

    @Test
    public void test_none() {
        Solution s = new Solution("RJ");
        Turn turn = new Turn("BV", 0, 0);
        boolean r = turn.compareTo(s);
        Assertions.assertThat(r).isFalse();
        Assertions.assertThat(turn.toString()).isEqualTo("|BV| 0 | 0 | 0/0 |");
    }

    @Test
    public void test_exact_and_same() {
        Solution s = new Solution("RVJ");
        Turn turn = new Turn("RBV", 0, 0);
        boolean r = turn.compareTo(s);
        Assertions.assertThat(r).isFalse();
        Assertions.assertThat(turn.toString()).isEqualTo("|RBV| 1 | 1 | 0/0 |");
    }

    @Test
    public void test_not_same_size() {
        Solution s = new Solution("R");
        Turn turn = new Turn("BV", 0, 0);
        Assertions.assertThatThrownBy(() -> turn.compareTo(s))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Non comparable");
    }
}